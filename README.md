## Clone the repo
$ git clone https://rajusingh100@bitbucket.org/rajusingh100/demoapp.git
$ cd demoapp

## Create a new directory and cd into it:
$ mkdir app
$ cd app

## Build and Deploy
# Build the container on your local machine
$ docker build -t {username}/helloworld-python .

# Push the container to docker registry
$ docker push {username}/helloworld-python

## Deploy the app into your cluste
$ kubectl apply --filename service.yaml

## Deployment details
$ kubectl get svc helloworld-python